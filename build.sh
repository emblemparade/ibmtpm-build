#!/bin/bash
set -e

HERE=$(dirname "$(readlink -f "$0")")

if [ ! -f "$HERE/tpm_server" ]; then

	# OpenSSL 1.0.2
	if [ ! -f "$HERE/prefix/lib/libssl.a" ]; then
		if [ ! -d "$HERE/openssl" ]; then
			git clone --branch OpenSSL_1_0_2-stable --depth=1 https://github.com/openssl/openssl.git
		fi
		cd "$HERE/openssl"
		./config --prefix="$HERE/prefix"
		make install -j$(nproc)
	fi

	# IBM TPM
	if [ ! -d "$HERE/src" ]; then
		if [ ! -f /tmp/ibmtpm974.tar.gz ]; then
			wget https://downloads.sourceforge.net/project/ibmswtpm2/ibmtpm974.tar.gz -O /tmp/ibmtpm974.tar.gz
		fi
		cd "$HERE"
		tar -zxvf /tmp/ibmtpm974.tar.gz
	fi
	cp "$HERE/makefile" "$HERE/src/"
	cd "$HERE/src"
	make -j$(nproc)
	cp tpm_server "$HERE"
	echo "Done: $HERE/tpm_server"

fi
